/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.WorkQueue;

import Business.Employee.Employee;
import Business.Quiz.Quiz;
import java.util.ArrayList;

/**
 *
 * @author raunak
 */
public class EducationEventWorkRequest extends WorkRequest{
    
    private String ngoName;
    private String instructorRequired;
    private String date;
    private String venue;
    
    private ArrayList<Employee> employeeList;
    private ArrayList<Employee> employeeWaitList;
    
    private Quiz quiz;
    
    public EducationEventWorkRequest(){
    this.employeeList = new ArrayList();
    this.employeeWaitList = new ArrayList();
    this.quiz = new Quiz();
    }

    public ArrayList<Employee> getEmployeeWaitList() {
        return employeeWaitList;
    }

    public void setEmployeeWaitList(ArrayList<Employee> employeeWaitList) {
        this.employeeWaitList = employeeWaitList;
    }
    
    
    
        
    public String getNgoName() {
        return ngoName;
    }

    public void setNgoName(String ngoName) {
        this.ngoName = ngoName;
    }

    
    public String getInstructorRequired() {
        return instructorRequired;
    }

    public void setInstructorRequired(String instructorRequired) {
        this.instructorRequired = instructorRequired;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getVenue() {
        return venue;
    }

    public void setVenue(String venue) {
        this.venue = venue;
    }

    public ArrayList<Employee> getEmployeeList() {
        System.err.print("getting");
        return employeeList;
    }

    public void setEmployeeList(ArrayList<Employee> employeeList) {
        this.employeeList = employeeList;
    }

    public Quiz getQuiz() {
        return quiz;
    }

    public void setQuiz(Quiz quiz) {
        this.quiz = quiz;
    }
    
    //    public void addEmployee(Employee emp){
//        employeeList.add(emp);
//    }
    
    
    @Override
    public String toString() {
        return ngoName+" ";
    }
}
