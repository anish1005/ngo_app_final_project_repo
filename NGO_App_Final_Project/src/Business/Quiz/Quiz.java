/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Quiz;

import java.util.ArrayList;

/**
 *
 * @author Anish Gupta
 */
public class Quiz {
    private ArrayList<Question> questions;    
    
    public Quiz( ) {
        this.questions = new ArrayList<>();
    }

    public ArrayList<Question> getQuestions() {
        return questions;
    }

    public void setQuestions(ArrayList<Question> questions) {
        this.questions = questions;
    }
       
    public Question createQuestion(int questionNumber, String question, String answer, int marks)
    {
     Question q = new Question();
     q.setQuestionNumber(questionNumber);
     q.setQuestion(question);
     q.setAnswer(answer);
     q.setMarks(marks);
     questions.add(q);
     return q;
    }
}
